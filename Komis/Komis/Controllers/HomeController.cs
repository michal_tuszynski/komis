﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Komis.Models;
using Komis.VIewModels;
using Microsoft.AspNetCore.Mvc;

namespace Komis.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICarRepository _carRepository;

        public HomeController(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }

        public IActionResult Index()
        {


            var cars = _carRepository.GetAllCars().OrderBy(s => s.Brand);
            var homeVM = new HomeVM()
            {
                Title = "Przegląd samochodów",
                Cars = cars.ToList()
        };
            return View(homeVM);
        }
    }
}