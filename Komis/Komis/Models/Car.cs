﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Komis.Models
{
    public class Car
    {
        public int Id { get; set; }
        public string Brand { get; set; }
        public string Type { get; set; }
        public int YearProduction { get; set; }
        public string Millage { get; set; }
        public string FuelType { get; set; }
        public string Power { get; set; }
        public string Discription { get; set; }
        public decimal Price { get; set; }
        public string FotoUrl { get; set; }
        public string MiniFotoUrl { get; set; }
        public bool IsCarOfWeek { get; set; }
    }
}
