﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Komis.Models
{
    public class MockCarRepository : ICarRepository
    {
        private List<Car> cars;

        public MockCarRepository()
        {
            if (cars == null)
            {
                LoadCars();
            }
        }

        private void LoadCars()
        {
            cars = new List<Car>
            {
                new Car {Id=1, Brand="Ford", Type="Mustang" ,YearProduction= 2016, Millage="98000 km", FuelType="benzyna" , Power="150 km", Price= 65000 , FotoUrl="", MiniFotoUrl="", Discription=" Bez uszkodzeń", IsCarOfWeek= false},
                new Car {Id=2, Brand="Citroen", Type="x5" ,YearProduction= 2015, Millage="108000 km", FuelType="diesel" , Power="200 km", Price= 75000 , FotoUrl="", MiniFotoUrl="", Discription=" Bez uszkodzeń", IsCarOfWeek= false},
                new Car {Id=3, Brand="BMW", Type="X5M" ,YearProduction= 2010, Millage="198000 km", FuelType="benzyna" , Power="350 km", Price= 95800 , FotoUrl="", MiniFotoUrl="", Discription="Drobne ślady użytkowania", IsCarOfWeek= false},
                new Car {Id=4, Brand="Ford", Type="Mustang" ,YearProduction= 2018, Millage="48000 km", FuelType="benzyna" , Power="850 km", Price= 285000 , FotoUrl="", MiniFotoUrl="", Discription="Prawdziwa perła, zadbany z niewiekim przebiegiem", IsCarOfWeek= true}
            };
        }

        public IEnumerable<Car> GetAllCars()
        {
            return cars;
        }

        public Car GetCar(int carId)
        {
            return cars.FirstOrDefault(s => s.Id == carId);
        }
    }
}
