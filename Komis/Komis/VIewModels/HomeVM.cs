﻿using Komis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Komis.VIewModels
{
    public class HomeVM
    {
        public string Title { get; set; }
        public List<Car> Cars { get; set; }
    }
}
